import lombok.extern.log4j.Log4j2;
import models.DoubleAvgAggregator;
import models.Hotel;
import models.Weather;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.state.KeyValueStore;
import serde.JsonSerDe;

import java.util.Objects;
import java.util.Optional;
import java.util.Properties;

@Log4j2
public class App {
  public static final String AGG_WEATHER_STORE_NAME = "avg-temp-per-key";
  public static final Serde<String> stringSerde = Serdes.String();
  public static final Serde<Double> doubleSerde = Serdes.Double();

  public static final JsonSerDe<Hotel> hotelSerDe = new JsonSerDe<>(Hotel.class);
  public static final JsonSerDe<Weather> weatherSerDe = new JsonSerDe<>(Weather.class);
  public static final JsonSerDe<DoubleAvgAggregator> doubleAggregatorSerDe = new JsonSerDe<>(DoubleAvgAggregator.class);

  /**
   * Stream of hotels does not have a Key. This method selects key from each record
   *
   * @param builder topology builder
   * @param topic   topic with hotel records
   * @return transformed stream of hotels per key (date+geohash)
   */
  public static KTable<String, Hotel> hotelsToTableTransformer(StreamsBuilder builder, String topic) {
    return builder.stream(topic, Consumed.with(Serdes.Void(), hotelSerDe))
      .filter((n, h) -> Objects.nonNull(h.getGeo_hash()))
      .selectKey((v, h) -> h.getDate() + ":" + h.getGeo_hash())
      .toTable(Materialized.with(stringSerde, hotelSerDe));
  }

  /**
   * Transforms weather stream
   *
   * @param builder topology builder
   * @param topic   topic with weather data
   * @return aggregated stream of average temperature per key (date+geohash)
   */
  public static KTable<String, DoubleAvgAggregator> weatherStreamToTableTransformer(StreamsBuilder builder, String topic) {
    return builder
      .stream(topic, Consumed.with(Serdes.Void(), weatherSerDe))
      .filter((v, w) -> Objects.nonNull(w.getGeo_hash()))
      .map((v, w) -> new KeyValue<>(w.getWthr_date() + ":" + w.getGeo_hash(), w.getAvg_tmpr_c()))
      .groupByKey(Grouped.with(stringSerde, doubleSerde))
      .aggregate(
        DoubleAvgAggregator::new,
        DoubleAvgAggregator::apply,
        Materialized.<String, DoubleAvgAggregator, KeyValueStore<Bytes, byte[]>>as(AGG_WEATHER_STORE_NAME)
          .withKeySerde(stringSerde)
          .withValueSerde(doubleAggregatorSerDe)
      );
  }

  public static KStream<String, Hotel> joinedTables(StreamsBuilder builder, String hotelsTopicName, String weatherTopicName) {
    return hotelsToTableTransformer(builder, hotelsTopicName)
      .leftJoin(
        weatherStreamToTableTransformer(builder, weatherTopicName),
        Hotel::applyWeatherConditions,
        Materialized.with(stringSerde, hotelSerDe))
      .toStream();
  }

  public static void main(String[] args) {
    Properties config = new Properties();
    config.put(StreamsConfig.APPLICATION_ID_CONFIG, "workshop-v1");
    config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG,
      Optional.ofNullable(System.getenv("BOOTSTRAP_SERVERS_CONFIG")).orElse("sandbox-hdp.hortonworks.com:6667")
    );

    config.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
    config.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
    config.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
    config.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
    config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
    config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");

    StreamsBuilder builder = new StreamsBuilder();

    joinedTables(builder, "hotels", "weather")
      .to("hotels-with-weather", Produced.with(stringSerde, hotelSerDe));

    Topology topology = builder.build();
    KafkaStreams streams = new KafkaStreams(topology, config);
    streams.start();

    Runtime.getRuntime().addShutdownHook(new Thread(() -> {
      try {
        streams.close();
        log.info("Stream stopped");
      } catch (Exception exc) {
        log.error("Got exception while executing shutdown hook: ", exc);
      }
    }));
  }
}
