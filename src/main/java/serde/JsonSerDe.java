package serde;

import lombok.AllArgsConstructor;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

@AllArgsConstructor
public class JsonSerDe<T> implements Serde<T> {

  final private Serializer<T> serializer;
  final private Deserializer<T> deserializer;

  public JsonSerDe(Class<T> claz) {
    this(new JsonSerializer<>(),new JsonDeserializer<>(claz));
  }

  @Override
  public Serializer<T> serializer() {
    return serializer;
  }

  @Override
  public Deserializer<T> deserializer() {
    return deserializer;
  }
}
