package serde;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.apache.kafka.common.serialization.Deserializer;

@Log4j2
public class JsonDeserializer<T> implements Deserializer<T> {
  private final ObjectMapper json = new ObjectMapper();
  private final Class<T> deserializedClass;

  public JsonDeserializer(Class<T> deserializedClass) {
    this.deserializedClass = deserializedClass;
  }

  @Override
  public T deserialize(String s, byte[] bytes) {
    try {
      return json.readValue(bytes, deserializedClass);
    } catch (Throwable th) {
      System.out.println(th.getMessage());
      return null;
    }
  }
}
