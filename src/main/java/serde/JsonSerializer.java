package serde;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.apache.kafka.common.serialization.Serializer;

@Log4j2
public class JsonSerializer<T> implements Serializer<T> {
  private static ObjectMapper json = new ObjectMapper();

  @Override
  public byte[] serialize(String s, T t) {
    try {
      return json.writeValueAsBytes(t);
    } catch (Throwable th) {
      log.warn(th);
      return null;
    }
  }
}
