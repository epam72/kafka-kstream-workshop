package models;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Data
public class DoubleAvgAggregator {
  long counter;
  double sum;

  public static DoubleAvgAggregator apply(String key, Double value, DoubleAvgAggregator agg) {
    agg.counter++;
    agg.sum += value;
    return agg;
  }
}
