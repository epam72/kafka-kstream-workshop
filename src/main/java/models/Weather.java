package models;

import lombok.*;

import java.time.LocalDate;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Weather {
  Double lat;
  Double lng;
  Double avg_tmpr_f;
  Double avg_tmpr_c;
  String wthr_date;
  Integer year;
  Integer month;
  Integer day;
  String geo_hash;
}
