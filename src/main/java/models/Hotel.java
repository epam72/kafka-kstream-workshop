package models;

import lombok.*;

import java.time.LocalDate;
import java.util.Optional;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Hotel {
  Long Id;
  String name;
  String Country;
  String City;
  String Address;
  Float Latitude;
  Float Longitude;
  String date;
  String geo_hash;
  Double avgTemperature;

  public Hotel applyWeatherConditions(DoubleAvgAggregator agg) {
    Optional.ofNullable(agg).ifPresent(a -> this.avgTemperature = agg.sum / agg.counter);
    return this;
  }
}
