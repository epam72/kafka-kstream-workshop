import models.DoubleAvgAggregator;
import models.Hotel;
import models.Weather;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.state.KeyValueStore;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;

public class WorkshopTests {
  private TopologyTestDriver testDriver;
  private final String inputHotelsTopicName = "inpHotels";
  private final String inputWeatherTopicName = "inpWeather";
  private final String outputTopicName = "out";

  @Before
  public void createEnv() {
    final Properties props = new Properties();

    props.setProperty(StreamsConfig.APPLICATION_ID_CONFIG, "app-test");
    props.setProperty(StreamsConfig.APPLICATION_ID_CONFIG, "kafka-workshop");
    props.setProperty(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "dummy:1234");

    StreamsBuilder builder = new StreamsBuilder();

    App.joinedTables(builder, inputHotelsTopicName, inputWeatherTopicName)
      .to(outputTopicName);

    final Topology topology = builder.build();
    testDriver = new TopologyTestDriver(topology, props);
  }

  @Test
  public void testAggKeyStoreAndOutputRecords() {
    String testGeoHash = "test";
    String testDate = LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE);

    Random rnd = new Random();
    int recCount = 10;
    List<Weather> wList = new ArrayList<>(recCount);

    for (int i = 0; i < recCount; i++) {
      wList.add(Weather.builder()
        .avg_tmpr_c(rnd.nextDouble())
        .wthr_date(testDate)
        .geo_hash(testGeoHash)
        .build());
    }

    Hotel hotel = Hotel.builder().geo_hash(testGeoHash).date(testDate).build();

    testDriver.createInputTopic(inputHotelsTopicName, Serdes.Void().serializer(), App.hotelSerDe.serializer())
      .pipeInput(hotel);

    testDriver.createInputTopic(inputWeatherTopicName, Serdes.Void().serializer(), App.weatherSerDe.serializer())
      .pipeValueList(wList);

    TestOutputTopic<String, Hotel> outputTopic = testDriver.createOutputTopic(outputTopicName, App.stringSerde.deserializer(), App.hotelSerDe.deserializer());

    KeyValueStore<String, DoubleAvgAggregator> state = testDriver.getKeyValueStore(App.AGG_WEATHER_STORE_NAME);

    // Now we should see at KeyValueStore one record with expected Average Value
    String newKey = testDate + ":" + testGeoHash;

    Assert.assertEquals(state.get(newKey).getCounter(), recCount);
    double expAvg = wList.stream().map(Weather::getAvg_tmpr_c).reduce(0.0d, (o, n) -> o += n) / recCount;
    Assert.assertEquals(expAvg, state.get(newKey).getSum() / state.get(newKey).getCounter(), 0.0001);

    // check final joined result
    List<KeyValue<String, Hotel>> values = outputTopic.readKeyValuesToList();

    // final topic is about to have all hotels and one more record with joined data
    Assert.assertEquals(recCount + 1, values.size());

    // check merged temperature with hotel
    Assert.assertEquals(expAvg, values.get(values.size() - 1).value.getAvgTemperature(), 0.0001);
  }

  @After
  public void tearDown() {
    testDriver.close();
  }
}
